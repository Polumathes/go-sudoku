package main

import (
	"fmt"
	"testing"
)

func TestSudoku(t *testing.T) {
	var sudokus []Sudoku

	sudokus = readSudokuFromFile("puzzle.txt")

	for sudokuIdx := range sudokus {
		fmt.Println("===============================")
		fmt.Println("Puzzle:")
		sudokus[sudokuIdx].Print(false)
		solved, metrics := Solve(&sudokus[sudokuIdx])
		if solved == true {
			fmt.Printf("Worst-case solution found in passes:%v, routines: %v\n",
				metrics.passes, metrics.routines)
		} else {
			fmt.Printf("Solution not found in %v passes:\n", metrics.passes)
		}
		sudokus[sudokuIdx].Print(true)
	}
	fmt.Println("===============================")
}
