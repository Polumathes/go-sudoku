package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"sync"
)

const (
	SUDOKU_ROW         int = 9
	SUDOKU_COL         int = 9
	SUDOKU_UNKNOWN     int = -1
	SUDOKU_CELL_WIDTH  int = 3
	SUDOKU_CELL_HEIGHT int = 3
)

type Sudoku struct {
	Board [SUDOKU_ROW][SUDOKU_COL]map[int]struct{}
}

type sudokuMetrics struct {
	passes    uint
	pivots  uint
	solutions []Sudoku
}

// Create a new sudoku board.
// The input argument must be a valid
// slice of sudoku numbers.
// Input: A slice of 9x9 integers.
// Output: A sudoku structure.
func NewSudoku(board *[]int) *Sudoku {
	var sudoku *Sudoku = nil

	if board != nil && len(*board) == (SUDOKU_ROW*SUDOKU_COL) {
		sudoku = new(Sudoku)
		sudoku.initBoard(*board)
	}

	return (sudoku)
}

// Perform a deep copy of one sudoku board
// into a new one. The new sudoku board
// will be freshly initialized in this function
// and returned.
// Input: A sudoku structure.
// Output: A new sudoku structure.
func Copy(sudoku *Sudoku) *Sudoku {
	copyOfSudoku := new(Sudoku)

	for rowIdx := 0; rowIdx < SUDOKU_ROW; rowIdx++ {
		for colIdx := 0; colIdx < SUDOKU_COL; colIdx++ {
			copyOfSudoku.Board[rowIdx][colIdx] = make(map[int]struct{})
			for key, _ := range sudoku.Board[rowIdx][colIdx] {
				copyOfSudoku.Board[rowIdx][colIdx][key] = struct{}{}
			}
		}
	}

	return copyOfSudoku
}

// Initializes the sudoku board with numbers.
// Any integer that is not a valid number
// will be interpreted as an unknown
// and inserted into the board as an unknown.
// Input: A slice of 9x9 integers.
func (sudoku *Sudoku) initBoard(board []int) {
	if len(board) != SUDOKU_ROW*SUDOKU_COL {
		return
	}

	inputIdx := 0
	for rowIdx := 0; rowIdx < SUDOKU_ROW; rowIdx++ {
		for colIdx := 0; colIdx < SUDOKU_COL; colIdx++ {

			sudoku.Board[rowIdx][colIdx] = make(map[int]struct{})
			number := board[inputIdx]

			if board[inputIdx] >= 1 && board[inputIdx] <= 10 {
				sudoku.Board[rowIdx][colIdx][number] = struct{}{}
			} else {
				sudoku.initPossibilities(rowIdx, colIdx)
			}

			inputIdx++
		}
	}
}

// All unknowns on a sudoku board have 9 possible numbers.
// This will initialize all possibilities in that sudoku coordinate.
// Input: The row and column index to be initialized.
func (sudoku *Sudoku) initPossibilities(rowIdx int, colIdx int) {
	for number := 1; number <= 9; number++ {
		sudoku.Board[rowIdx][colIdx][number] = struct{}{}
	}
}

// A helper function to get the first (and only) key
// in what should be a known number inside of a sudoku coordinate.
// Input: Coordinates expressed as row and column index
// Output: The first number in the sudoku coordinate.
func (sudoku *Sudoku) getFirstKey(rowIdx int, colIdx int) int {
	var key int = SUDOKU_UNKNOWN
	for key, _ = range sudoku.Board[rowIdx][colIdx] {
		break
	}

	return key
}

// Validate a 3x3 cell in the sudoku board.
// If there are any duplicate 'known' numbers in the cell
// then it will be invalidated.
// Input: The cell row and cell column to be validated.
func (sudoku *Sudoku) validateCell(cellCol int, cellRow int) bool {
	if cellCol >= 3 || cellCol < 0 || cellRow >= 3 || cellRow < 0 {
		return (false)
	}

	validNumbers := map[int]struct{}{1: {}, 2: {}, 3: {}, 4: {}, 5: {}, 6: {}, 7: {}, 8: {}, 9: {}}
	rowStart := cellRow * SUDOKU_CELL_HEIGHT
	colStart := cellCol * SUDOKU_CELL_WIDTH
	rowEnd := rowStart + SUDOKU_CELL_HEIGHT
	colEnd := colStart + SUDOKU_CELL_WIDTH

	for rowIdx := rowStart; rowIdx < rowEnd; rowIdx++ {
		for colIdx := colStart; colIdx < colEnd; colIdx++ {
			if len(sudoku.Board[rowIdx][colIdx]) > 1 {
				continue
			}

			key := sudoku.getFirstKey(rowIdx, colIdx)

			_, found := validNumbers[key]
			if found == true {
				delete(validNumbers, key)
			} else {
				// If the number isn't found, it's a duplicate.
				// That means this cell isn't valid.
				return (false)
			}
		}
	}

	return true
}

// Validates the row. If any of the known
// row values are duplicates, the row is invalid.
// Input: The row to inspect.
// Output: true if valid, false if invalid.
func (sudoku *Sudoku) validateRow(row int) bool {
	if row >= 9 || row < 0 {
		return (false)
	}

	validNumbers := map[int]struct{}{1: {}, 2: {}, 3: {}, 4: {}, 5: {}, 6: {}, 7: {}, 8: {}, 9: {}}

	for colIdx := 0; colIdx < SUDOKU_COL; colIdx++ {
		if len(sudoku.Board[row][colIdx]) > 1 {
			continue
		}

		key := sudoku.getFirstKey(row, colIdx)

		_, found := validNumbers[key]
		if found == true {
			delete(validNumbers, key)
		} else {
			// If the number isn't found, it's a duplicate.
			// That means this column isn't valid.
			return (false)
		}
	}

	return true
}

// Validates the column. If any of the known
// column values are duplicates, the column is invalid.
// Input: The column to inspect.
// Output: true if valid, false if invalid.
func (sudoku *Sudoku) validateCol(col int) bool {
	if col >= 9 || col < 0 {
		return (false)
	}

	validNumbers := map[int]struct{}{1: {}, 2: {}, 3: {}, 4: {}, 5: {}, 6: {}, 7: {}, 8: {}, 9: {}}

	for rowIdx := 0; rowIdx < SUDOKU_ROW; rowIdx++ {
		if len(sudoku.Board[rowIdx][col]) > 1 {
			continue
		}

		key := sudoku.getFirstKey(rowIdx, col)

		_, found := validNumbers[key]
		if found == true {
			delete(validNumbers, key)
		} else {
			// If the number isn't found, it's a duplicate.
			// That means this row isn't valid.
			return (false)
		}
	}

	return true
}

// Returns the number of unknowns remaining. These
// are sudoku coordinates that have not yet been solved.
// Output: The total number of sudoku coordinates
// that have not yet been solved.
func (sudoku *Sudoku) NumUnknownsRemaining() int {
	unknownsRemaining := 0
	for rowIdx := 0; rowIdx < SUDOKU_ROW; rowIdx++ {
		for colIdx := 0; colIdx < SUDOKU_COL; colIdx++ {
			if len(sudoku.Board[rowIdx][colIdx]) > 1 {
				unknownsRemaining++
			}
		}
	}

	return unknownsRemaining
}

// Validates the entire sudoku board.
// If the row, column or cell has duplicate
// values then the board is invalid.
// Output: Return true if valid or false if invalid.
func (sudoku *Sudoku) ValidateBoard() bool {
	for cellRowIdx := 0; cellRowIdx < SUDOKU_CELL_HEIGHT; cellRowIdx++ {
		for cellColIdx := 0; cellColIdx < SUDOKU_CELL_WIDTH; cellColIdx++ {
			if sudoku.validateCell(cellColIdx, cellRowIdx) == false {
				return false
			}
		}
	}

	for colIdx := 0; colIdx < SUDOKU_COL; colIdx++ {
		if sudoku.validateCol(colIdx) == false {
			return false
		}
	}

	for rowIdx := 0; rowIdx < SUDOKU_ROW; rowIdx++ {
		if sudoku.validateRow(rowIdx) == false {
			return false
		}
	}

	return true
}

// Eliminates the possibilites at a specific coordinate
// by inspecting all of the known values in that row.
// Input: The row and column index of the sudoku coordinate.
// Output: True if progress was made and >= 1 possibility was removed.
// False if no possibilities were removed from the sudoku coordinate.
func (sudoku *Sudoku) eliminateKnownRowPossibilitesFromCoordinate(selectedRowIdx int, selectedColIdx int) bool {
	var rc bool = false

	if selectedRowIdx < 0 || selectedRowIdx >= 9 || selectedColIdx < 0 || selectedColIdx >= 9 {
		return rc
	}

	possibles := sudoku.Board[selectedRowIdx][selectedColIdx]
	if len(possibles) == 1 {
		return rc
	}

	for colIdx := 0; colIdx < SUDOKU_COL; colIdx++ {
		known := sudoku.Board[selectedRowIdx][colIdx]
		if colIdx == selectedColIdx || len(known) > 1 {
			continue
		}

		knownValue := sudoku.getFirstKey(selectedRowIdx, colIdx)
		_, found := possibles[knownValue]

		if found == true {
			delete(possibles, knownValue)
			rc = true
			if len(possibles) == 1 {
				break
			}
		}
	}

	return rc
}

// Traverses each sudoku coordinate in a given row
// and eliminates possibilities from each.
// Input: The row to traverse and eliminate possibilities.
// Output: True if progress was made and >= 1 possibility was removed.
// False if no possibilities were removed from the row.
func (sudoku *Sudoku) eliminateAllKnownRowPossibilites(rowIdx int) bool {
	//var rc bool = false
	rc := false
	if rowIdx < 0 || rowIdx >= 9 {
		return false
	}

	for selectedColIdx := 0; selectedColIdx < SUDOKU_COL; selectedColIdx++ {
		rc = sudoku.eliminateKnownRowPossibilitesFromCoordinate(rowIdx, selectedColIdx) || rc
	}

	return rc
}

// Eliminates the possibilites at a specific coordinate
// by inspecting all of the known values in that column.
// Input: The row and column index of the sudoku coordinate.
// Output: True if progress was made and >= 1 possibility was removed.
// False if no possibilities were removed from the sudoku coordinate.
func (sudoku *Sudoku) eliminateKnownColPossibilitesFromCoordinate(selectedRowIdx int, selectedColIdx int) bool {
	var rc bool = false

	if selectedRowIdx < 0 || selectedRowIdx >= 9 || selectedColIdx < 0 || selectedColIdx >= 9 {
		return rc
	}

	possibles := sudoku.Board[selectedRowIdx][selectedColIdx]
	if len(possibles) == 1 {
		return rc
	}

	for rowIdx := 0; rowIdx < SUDOKU_ROW; rowIdx++ {
		known := sudoku.Board[rowIdx][selectedColIdx]
		if rowIdx == selectedRowIdx || len(known) > 1 {
			continue
		}

		knownValue := sudoku.getFirstKey(rowIdx, selectedColIdx)
		_, found := possibles[knownValue]

		if found == true {
			delete(possibles, knownValue)
			rc = true
			if len(possibles) == 1 {
				break
			}
		}
	}

	return rc
}

// Traverses each sudoku coordinate in a given row
// and eliminates possibilities from each.
// Input: The column to traverse and eliminate possibilities.
// Output: True if progress was made and >= 1 possibility was removed.
// False if no possibilities were removed from the column.
func (sudoku *Sudoku) eliminateAllKnownColPossibilites(colIdx int) bool {
	var rc bool = false

	if colIdx < 0 || colIdx >= 9 {
		return false
	}

	for selectedRowIdx := 0; selectedRowIdx < SUDOKU_ROW; selectedRowIdx++ {
		rc = sudoku.eliminateKnownColPossibilitesFromCoordinate(selectedRowIdx, colIdx) || rc
	}

	return rc
}

// Eliminates the possibilites at a specific coordinate
// by inspecting all of the known values in that cell.
// Input: The row and column index of the sudoku coordinate.
// Output: True if progress was made and >= 1 possibility was removed.
// False if no possibilities were removed from the sudoku coordinate.
func (sudoku *Sudoku) eliminateKnownCellPossibilitesFromCoordinate(selectedRowIdx int, selectedColIdx int) bool {
	var rc bool = false
	if selectedRowIdx < 0 || selectedRowIdx >= 9 || selectedColIdx < 0 || selectedColIdx >= 9 {
		return rc
	}

	possibles := sudoku.Board[selectedRowIdx][selectedColIdx]
	if len(possibles) == 1 {
		return rc
	}

	cellCol := int(selectedColIdx / SUDOKU_CELL_WIDTH)
	cellRow := int(selectedRowIdx / SUDOKU_CELL_HEIGHT)
	rowStart := cellRow * SUDOKU_CELL_HEIGHT
	colStart := cellCol * SUDOKU_CELL_WIDTH
	rowEnd := rowStart + SUDOKU_CELL_HEIGHT
	colEnd := colStart + SUDOKU_CELL_WIDTH

	for rowIdx := rowStart; rowIdx < rowEnd; rowIdx++ {
		if len(possibles) == 1 {
			break
		}
		for colIdx := colStart; colIdx < colEnd; colIdx++ {
			if rowIdx == selectedRowIdx && colIdx == selectedColIdx {
				continue
			}

			if len(sudoku.Board[rowIdx][colIdx]) > 1 {
				continue
			}

			knownValue := sudoku.getFirstKey(rowIdx, colIdx)
			_, found := possibles[knownValue]

			if found == true {
				delete(possibles, knownValue)
				rc = true

				if len(possibles) == 1 {
					break
				}
			}
		}
	}

	return rc
}

// Traverses each sudoku coordinate in a given cell
// and eliminates possibilities from each.
// Output: True if progress was made and >= 1 possibility was removed.
// False if no possibilities were removed from the cell.
func (sudoku *Sudoku) eliminateAllKnownCellPossibilites(cellRow int, cellCol int) bool {
	var rc bool = false

	if cellRow < 0 || cellRow >= 3 || cellCol < 0 || cellCol >= 3 {
		return false
	}

	rowStart := cellRow * SUDOKU_CELL_HEIGHT
	colStart := cellCol * SUDOKU_CELL_WIDTH
	rowEnd := rowStart + SUDOKU_CELL_HEIGHT
	colEnd := colStart + SUDOKU_CELL_WIDTH

	for selectedRowIdx := rowStart; selectedRowIdx < rowEnd; selectedRowIdx++ {
		for selectedColIdx := colStart; selectedColIdx < colEnd; selectedColIdx++ {
			rc = sudoku.eliminateKnownCellPossibilitesFromCoordinate(selectedRowIdx, selectedColIdx) || rc
		}
	}

	return rc
}

// Eliminates possibilites by traversing each coordinate
// in a sudoku puzzle. Comparisons are then made to
// the row, column and cell of that coordinate so
// that the possibilities of a particular coordinate are eliminated.
// Output: True if progress was made and >= 1 possibility was removed.
// False if no possibilities were removed from the puzzle.
func (sudoku *Sudoku) eliminateKnownPossibilities() bool {
	var rc bool = false

	for rowIdx := 0; rowIdx < SUDOKU_ROW; rowIdx++ {
		rc = sudoku.eliminateAllKnownRowPossibilites(rowIdx) || rc
	}

	for colIdx := 0; colIdx < SUDOKU_COL; colIdx++ {
		rc = sudoku.eliminateAllKnownColPossibilites(colIdx) || rc
	}

	for cellRowIdx := 0; cellRowIdx < SUDOKU_CELL_HEIGHT; cellRowIdx++ {
		for cellColIdx := 0; cellColIdx < SUDOKU_CELL_WIDTH; cellColIdx++ {
			rc = sudoku.eliminateAllKnownCellPossibilites(cellRowIdx, cellColIdx) || rc
		}
	}

	return rc
}

// Collects all possibilities in the row of a given
// sudoku coordinate. Only coordinates that are unsolved/unknown
// will be collected and the current sudoku coordinate will be exempted
// from collection.
// Input: The sudoku coordinate to inspect in row index and column index.
// Output: A map of all cumulative possibilities in the row, note the exemptions
// listed above.
func (sudoku *Sudoku) collectRowPossibilities(rowIdx int, colIdx int) map[int]struct{} {
	var cumulativePossibilities map[int]struct{} = map[int]struct{}{}

	if rowIdx < 0 || rowIdx >= 9 || colIdx < 0 || colIdx >= 9 {
		return cumulativePossibilities
	}

	for otherColIdx := 0; otherColIdx < SUDOKU_COL; otherColIdx++ {
		currentPossibilities := sudoku.Board[rowIdx][otherColIdx]

		if colIdx == otherColIdx || len(currentPossibilities) == 0 {
			continue
		}

		for key, _ := range currentPossibilities {
			cumulativePossibilities[key] = struct{}{}
		}

	}

	return cumulativePossibilities
}

// Collects all possibilities in the column of a given
// sudoku coordinate. Only coordinates that are unsolved/unknown
// will be collected and the current sudoku coordinate will be exempted
// from collection.
// Input: The sudoku coordinate to inspect in row index and column index.
// Output: A map of all cumulative possibilities in the column, note the exemptions
// listed above.
func (sudoku *Sudoku) collectColPossibilities(rowIdx int, colIdx int) map[int]struct{} {
	var cumulativePossibilities map[int]struct{} = map[int]struct{}{}

	if rowIdx < 0 || rowIdx >= 9 || colIdx < 0 || colIdx >= 9 {
		return cumulativePossibilities
	}

	for otherRowIdx := 0; otherRowIdx < SUDOKU_ROW; otherRowIdx++ {
		currentPossibilities := sudoku.Board[otherRowIdx][colIdx]

		if rowIdx == otherRowIdx || len(currentPossibilities) == 0 {
			continue
		}

		for key, _ := range currentPossibilities {
			cumulativePossibilities[key] = struct{}{}
		}

	}

	return cumulativePossibilities
}

// Collects all possibilities in the cell of a given
// sudoku coordinate. Only coordinates that are unsolved/unknown
// will be collected and the current sudoku coordinate will be exempted
// from collection.
// Input: The sudoku coordinate to inspect in row index and column index.
// Output: A map of all cumulative possibilities in the cell, note the exemptions
// listed above.
func (sudoku *Sudoku) collectCellPossibilities(rowIdx int, colIdx int) map[int]struct{} {
	var cumulativePossibilities map[int]struct{} = map[int]struct{}{}

	if rowIdx < 0 || rowIdx >= 9 || colIdx < 0 || colIdx >= 9 {
		return cumulativePossibilities
	}

	cellCol := int(colIdx / SUDOKU_CELL_WIDTH)
	cellRow := int(rowIdx / SUDOKU_CELL_HEIGHT)
	rowStart := cellRow * SUDOKU_CELL_HEIGHT
	colStart := cellCol * SUDOKU_CELL_WIDTH
	rowEnd := rowStart + SUDOKU_CELL_HEIGHT
	colEnd := colStart + SUDOKU_CELL_WIDTH

	for cellRowIdx := rowStart; cellRowIdx < rowEnd; cellRowIdx++ {
		for cellColIdx := colStart; cellColIdx < colEnd; cellColIdx++ {
			currentPossibilities := sudoku.Board[cellRowIdx][cellColIdx]

			if rowIdx == cellRowIdx && colIdx == cellColIdx {
				continue
			}

			if len(currentPossibilities) == 1 {
				continue
			}

			for key, _ := range currentPossibilities {
				cumulativePossibilities[key] = struct{}{}
			}
		}
	}

	return cumulativePossibilities
}

// Collects all possibilities in the row, column and cell of a given
// sudoku coordinate. Only coordinates that are unsolved/unknown
// will be collected and the current sudoku coordinate will be exempted
// from collection.
// Input: The sudoku coordinate to inspect in row index and column index.
// Output: A map of all cumulative possibilities in the column,
// row and cell, note the exemptions listed above.
func (sudoku *Sudoku) collectAllPotentialForCoordinate(rowIdx int, colIdx int) map[int]struct{} {
	cumulativePossibilities := sudoku.collectRowPossibilities(rowIdx, colIdx)

	for key, _ := range sudoku.collectColPossibilities(rowIdx, colIdx) {
		cumulativePossibilities[key] = struct{}{}
	}

	for key, _ := range sudoku.collectCellPossibilities(rowIdx, colIdx) {
		cumulativePossibilities[key] = struct{}{}
	}

	return cumulativePossibilities
}

// Collects all of the potentials for the row, column and cell
// of a given sudoku coordinate. If one of the coordinate's potential
// values aren't found in the comparing list (map) of potential values,
// then it can be deduced that the solution for this coordinate
// is the unique number not found in the comparable list of potentials.
// Note that coordinates with solutions will be skipped, given that
// they are already solved.
// Input: The sudoku coodinate expressed as the row index and column index.
// Output: True if a solution was determined. False if no solution
// was found for the given coordinate.
func (sudoku *Sudoku) deduceAllPotentialForCoordinate(rowIdx int, colIdx int) bool {
	var rc bool = false
	currentPotential := sudoku.Board[rowIdx][colIdx]

	if len(currentPotential) == 1 {
		return false
	}

	allOtherPotential := sudoku.collectAllPotentialForCoordinate(rowIdx, colIdx)

	for key, _ := range currentPotential {
		_, found := allOtherPotential[key]
		if found == false {
			sudoku.Board[rowIdx][colIdx] = map[int]struct{}{}
			sudoku.Board[rowIdx][colIdx][key] = struct{}{}
			rc = true
			break
		}
	}

	return rc
}

// Loops through all coordinates in the sudoku puzzle
// and attempts to find a solution for the coordinate
// by comparing its list of possibilities against the cumulative
// list of row, column and cell possibilities of that coordinate.
// This is a proceess of deduction of possibilities by means
// of cross referencing with XOR logic.
// Output: True if and sudoku coordinate has been found to have a
// solution. False if no sudoku coordinate was found to have a solution.
func (sudoku *Sudoku) eliminateDeducedPossibilities() bool {
	var rc bool = false
	for rowIdx := 0; rowIdx < SUDOKU_ROW; rowIdx++ {
		for colIdx := 0; colIdx < SUDOKU_COL; colIdx++ {
			rc = sudoku.deduceAllPotentialForCoordinate(rowIdx, colIdx) || rc
		}
	}

	return rc
}

// Determines if the sudoku puzzle is finished.
// The definition of finished is that all coordinates
// have a single value applied.
// Note that finished is not the same as solved.
// A puzzle is solved if every coordinate has a single
// value and if the end result is valid.
// Output: True if there are no more possibilities left in
// any of the sudoku coordinates. False if there is at least
// 1 sudoku coordinate left with a list (map) of possibilities.
func (sudoku *Sudoku) IsFinished() bool {
	for rowIdx := 0; rowIdx < SUDOKU_ROW; rowIdx++ {
		for colIdx := 0; colIdx < SUDOKU_COL; colIdx++ {
			if len(sudoku.Board[rowIdx][colIdx]) > 1 {
				return false
			}
		}
	}

	return true
}

// The "ultra-hard" Sudoku puzzles may have
// a sudoku coordinate with 2 or more possibilities that
// can no longer be reduced to a single answer. In this scenario,
// it is necessary to solve for 2 or more possible sudoku puzzles.
// This function can be recursively called and likely will be,
// the hardest sudoku puzzles require multiple pivots into multiple
// sudoku sub-puzzles. Only a valid solution for a sudoku puzzle should
// have exactly 1 valid outcome, more than 1 outcome implies that the
// sudoku puzzle is flawed.
// Input: A sudoku puzzle, presumably valid.
// Output: Multiple outputs, organized in a single
// structure (container). The number of passes across
// the entire sudoku board for known values + possibility deduction.
// The number of go-pivots created as a result of forking
// multiple solution computations. A list of valid sudoku solutions
// found.
func forkSolution(sudoku *Sudoku) (metrics *sudokuMetrics) {
	var smallestCoordRowIdx int
	var smallestCoordColIdx int
	var smallestPotential int = 10
	metrics = new(sudokuMetrics)
	metrics.passes = 0
	metrics.pivots = 0

	for rowIdx := 0; rowIdx < SUDOKU_ROW; rowIdx++ {
		for colIdx := 0; colIdx < SUDOKU_COL; colIdx++ {
			if len(sudoku.Board[rowIdx][colIdx]) > 1 && len(sudoku.Board[rowIdx][colIdx]) < smallestPotential {
				smallestPotential = len(sudoku.Board[rowIdx][colIdx])
				smallestCoordRowIdx = rowIdx
				smallestCoordColIdx = colIdx
			}
		}
	}

	fanSize := len(sudoku.Board[smallestCoordRowIdx][smallestCoordColIdx])
	fannedMetrics := make([]sudokuMetrics, fanSize)

	forkSolve := func(forkedSudoku *Sudoku, fanMetric *sudokuMetrics, wg *sync.WaitGroup) {
		defer wg.Done()
		_, metrics := Solve(forkedSudoku)
		fanMetric.passes += metrics.passes
		fanMetric.pivots += metrics.pivots
		fanMetric.solutions = append(fanMetric.solutions, metrics.solutions...)
	}

	var wg sync.WaitGroup
	fannedMetricsIdx := 0

	for key, _ := range sudoku.Board[smallestCoordRowIdx][smallestCoordColIdx] {
		forkSudoku := Copy(sudoku)
		forkSudoku.Board[smallestCoordRowIdx][smallestCoordColIdx] = map[int]struct{}{}
		forkSudoku.Board[smallestCoordRowIdx][smallestCoordColIdx][key] = struct{}{}

		wg.Add(1)

		go forkSolve(forkSudoku, &fannedMetrics[fannedMetricsIdx], &wg)
		fannedMetricsIdx++
	}

	wg.Wait()

	for fannedMetricsIdx = 0; fannedMetricsIdx < fanSize; fannedMetricsIdx++ {
		metrics.passes += fannedMetrics[fannedMetricsIdx].passes
		metrics.pivots += fannedMetrics[fannedMetricsIdx].pivots
		metrics.solutions = append(metrics.solutions, fannedMetrics[fannedMetricsIdx].solutions...)
	}

	return metrics
}

// Solves a sudoku puzzle or determines if there is no valid solution.
// Output: Solved means that the puzzle is finished and that the
// solution is valid. The definition of a valid solution is exactly
// 1 valid and fully resolved sudoku board is returned.
// Metrics will return the metrics to compute the performance
// in finding the solution or determinining that there isn't a solution.
// The metrics will contain a list of all valid sudoku solutions, for
// a proper solution there should be exactly 1 returned.
func Solve(sudoku *Sudoku) (solved bool, metrics *sudokuMetrics) {
	metrics = new(sudokuMetrics)
	metrics.passes = uint(0)
	metrics.pivots = uint(1)
	solved = false

	if sudoku.ValidateBoard() == false {
		return solved, metrics
	}

	for progress := true; progress != false; {
		progress = false
		previousPasses := metrics.passes

		for rc := sudoku.eliminateKnownPossibilities(); rc == true; rc = sudoku.eliminateKnownPossibilities() {
			metrics.passes++
		}

		for rc := sudoku.eliminateDeducedPossibilities(); rc == true; rc = sudoku.eliminateDeducedPossibilities() {
			metrics.passes++
		}

		if previousPasses < metrics.passes {
			previousPasses = metrics.passes
			progress = true
		}
	}

	if sudoku.ValidateBoard() == false {
		return solved, metrics
	}

	if sudoku.IsFinished() == true {
		metrics.solutions = append(metrics.solutions, *sudoku)
		solved = true
		return solved, metrics
	}

	// The puzzle is still valid but no solution has yet been found.
	// This means that there exists multiple outcomes for this puzzle.
	// 1) If there are multiple correct solutions, then this puzzle is invalid.
	// 2) If there are no possible solutions, then this puzzle is invalid.
	// 3) If there exists exactly 1 solution with all other possibilities
	// having been explored, then this puzzle is solved.
	forkedMetrics := forkSolution(sudoku)
	metrics.passes += forkedMetrics.passes
	metrics.pivots += forkedMetrics.pivots
	metrics.solutions = append(metrics.solutions, forkedMetrics.solutions...)

	if len(metrics.solutions) == 1 {
		solved = true
		sudoku.Board = metrics.solutions[0].Board
		return solved, metrics
	}

	return solved, metrics
}

// Prints the sudoku puzzle to CLI.
// Input: printPossibles is true if the number of
// possibilities is to be printed in an unresolved
// sudoku coordinate. EG: [5] at a position in the sudoku
// printed puzzle means that this sudoku coordinate
// contains 5 remaining possibilities. The printPossibles
// is false if instead of the possible values, a # rune
// is printed instead.
func (sudoku *Sudoku) Print(printPossibles bool) {
	// Start at 0, it will never be evaluated at 0 except for
	// setting it here. This is to compensate the initial
	// state of n = 0 when printing.
	cellCeilCount := 0
	cellWallCount := 0

	// Loop through all of the rows.
	// Start from the top and proceed downwards.
	for rowIdx := 0; rowIdx < SUDOKU_ROW; rowIdx++ {

		// Special condition. For every 3 rows starting at row 0, print an entire
		// row of: +---------+---------+---------+
		if cellCeilCount%3 == 0 {
			cellCeilCount = 0
			for colIdx := 0; colIdx < SUDOKU_COL; colIdx++ {
				// Another special condition.
				// Print + at every juncture.
				if cellWallCount%3 == 0 {
					fmt.Print("+")
					cellWallCount = 0
				}
				fmt.Print("---")
				cellWallCount++
			}
			cellCeilCount = 0
			fmt.Println("+")
		}
		cellCeilCount++

		// Loop from the left and print to the right -->-->-->
		for colIdx := 0; colIdx < SUDOKU_COL; colIdx++ {
			// A special condition.
			// Print a wall symbol | for every 3 symbols printed,
			// starting with 0 symbols printed.
			if cellWallCount%3 == 0 {
				fmt.Print("|")
				cellWallCount = 0
			}
			cellWallCount++

			// This is where the printing of the numbers happen
			if len(sudoku.Board[rowIdx][colIdx]) > 1 {
				if printPossibles == true {
					fmt.Printf("[%v]", len(sudoku.Board[rowIdx][colIdx]))
				} else {
					fmt.Print(" # ")
				}
			} else {
				key := sudoku.getFirstKey(rowIdx, colIdx)
				fmt.Printf(" %v ", strconv.Itoa(key))
			}
		}
		cellCeilCount++
		// Print a wall | symbol at the very end of every row
		fmt.Print("|")
		fmt.Println("")
	}

	// Finally, everything has been printed.
	// Now print one last row of +---------+---------+---------+
	// before exiting. Comments above apply to here as well.
	cellWallCount = 0
	for colIdx := 0; colIdx < SUDOKU_COL; colIdx++ {
		if cellWallCount%3 == 0 {
			fmt.Print("+")
			cellWallCount = 0
		}
		fmt.Print("---")
		cellWallCount++
	}
	// End it with the connecting + symbol.
	fmt.Println("+")
}

// Reads a file and translates it into list of sudoku
// puzzles to be solved.
// Format:
// 5 3 0   0 7 0   0 0 0
// 6 0 0   1 9 5   0 0 0
// 0 9 8   0 0 0   0 6 0
//
// 8 0 0   0 6 0   0 0 3
// 4 0 0   8 0 3   0 0 1
// 7 0 0   0 2 0   0 0 6
//
// 0 6 0   0 0 0   2 8 0
// 0 0 0   4 1 9   0 0 5
// 0 0 0   0 8 0   0 7 9
// Spaces are used as a delimeter between valid sudoku
// values to be used in the puzzle. Any space separated
// symbol or series of symbols that isn't 1<=X<=9 will
// be interpreted as an unknown and inserted into the
// possible as a set of 9 possibilities for that sudoku coordinate.
// Input: The file path that contains the information
// to create 0 or more sudoku puzzles.
// Output: The list of sudoku puzzles to be solved.
func readSudokuFromFile(filename string) []Sudoku {
	var sudokus []Sudoku

	file, err := os.Open(filename)
	if err != nil {
		fmt.Printf("Could not open file: %v\n", err)
		return sudokus
	}

	numbers := []int{}
	reader := bufio.NewReader(file)
	for line, err := "", (error)(nil); err == nil; line, err = reader.ReadString('\n') {
		text := strings.TrimRight(line, "\r\n")
		data := strings.Split(text, " ")

		for numberIdx := range data {
			number, err := strconv.Atoi(data[numberIdx])
			if err != nil {
				continue
			}

			numbers = append(numbers, number)
			if len(numbers) == SUDOKU_COL*SUDOKU_ROW {
				sudoku := NewSudoku(&numbers)
				if sudoku != nil {
					sudokus = append(sudokus, *sudoku)
				}
				numbers = []int{}
			}
		}
	}

	fmt.Printf("Loaded %v sudoku puzzles\n", len(sudokus))
	return sudokus
}

func main() {
	// Supply a filepath argument.
	args := os.Args[1:]
	var sudokus []Sudoku

	if len(args) == 1 {
		sudokus = readSudokuFromFile(args[0])
	}

	for sudokuIdx := range sudokus {
		fmt.Println("===============================")
		fmt.Println("Puzzle:")
		sudokus[sudokuIdx].Print(false)
		solved, metrics := Solve(&sudokus[sudokuIdx])
		if solved == true {
			fmt.Printf("Worst-case solution found in passes: %v, pivots: %v\n",
				metrics.passes, metrics.pivots)
		} else {
			fmt.Printf("Solution not found in passes: %v, pivots: %v\n",
				metrics.passes, metrics.pivots)
		}
		sudokus[sudokuIdx].Print(true)
	}
	fmt.Println("===============================")
}
